package com.barosanu.controller;

import com.barosanu.model.EmailAccountBean;
import com.barosanu.model.EmailMessageBean;
import com.barosanu.model.folder.EmailFolderBean;
import com.barosanu.model.folder.Folder;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ModelAccess {

    private static MainController mainController;

    private Map<String, EmailAccountBean> emailAccounts = new HashMap<String,EmailAccountBean>();

    private Map<String,String> emailsFailedToConnect = new HashMap<>();

    private ArrayList<String> failedUsername = new ArrayList<>();

    private ObservableList<String> emailAccountNames = FXCollections.observableArrayList();

    public ObservableList<String> getEmailAccountNames() {
        return emailAccountNames;
    }

    /**
     * This is a temporal solution
     * @return
     */

    public static MainController getMainController() {
        return mainController;
    }

    public static void setMainController(MainController mainController) {
        ModelAccess.mainController = mainController;
    }

    public EmailAccountBean getEmailAccountsByName(String Name) {
        return emailAccounts.get(Name);
    }

    public void addAccount(EmailAccountBean account){
        emailAccounts.put(account.getEmailAddress() , account);
        emailAccountNames.add(account.getEmailAddress());
    }

    private EmailMessageBean selectedMessage;

    private EmailFolderBean<String> selectedFolder;

    public EmailFolderBean<String> getSelectedFolder() {
        return selectedFolder;
    }

    public void setSelectedFolder(EmailFolderBean<String> selectedFolder) {
        this.selectedFolder = selectedFolder;
    }

    public EmailMessageBean getSelectedMessage() {
        return selectedMessage;
    }

    public void setSelectedMessage(EmailMessageBean selectedMessage) {
        this.selectedMessage = selectedMessage;
    }


    //we want reference to all folders we have in the project
    private List<Folder> foldersList = new ArrayList<Folder>();

    public List<Folder> getFoldersList(){
        return foldersList;
    }

    public void addFolder(Folder folder){
        foldersList.add(folder);
    }

    public void addFailedEmail(String username,String password){
        emailsFailedToConnect.put(username,password);
        failedUsername.add(username);
    }

    public void removeFailedEmail(String username){
        emailsFailedToConnect.remove(username);
        failedUsername.remove(username);
    }

    public String getPassword(String username){
        return emailsFailedToConnect.get(username);
    }

    public ArrayList<String> getFailedUsername(){
        return failedUsername;
    }




}

