package com.barosanu.controller;

import com.barosanu.App;
import com.barosanu.model.EmailConstants;
import com.barosanu.services.CreateAndRegisterEmailAccountService;
import com.barosanu.services.IMAPServices.IMAPRetrievalService;
import com.barosanu.services.ProtocolRequirementsService;
import com.barosanu.services.pop3services.POP3DownloadService;
import com.barosanu.view.ViewFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.stage.Stage;

import java.net.URL;
import java.util.ResourceBundle;

public class AddANewAccountController extends AbstractController implements Initializable {


    @FXML
    private RadioButton popRadioBtn;

    @FXML
    private RadioButton imapRadioBtn;

    @FXML
    private TextField passwordTextField;

    @FXML
    private Label passwordSideLabel;

    @FXML
    private Label nameSideLabel;

    @FXML
    private Label emailSideLabel;

    @FXML
    private Button passwordButton;

    @FXML
    private TextField nameField;

    @FXML
    private TextField emailAddressField;

    @FXML
    private PasswordField passwordField;

    @FXML
    private CheckBox rememberPasswordCheckBox;

    @FXML
    private Button cancelBtn;

    @FXML
    private Button addBtn;

    @FXML
    private Label messageLabel;

    @FXML
    public void cancelBtnAction( ){
        //TODO check this code :
        Stage stage = (Stage) cancelBtn.getScene().getWindow();
        ViewFactory.removeWindow(stage);
        stage.close();
        System.out.println("cancel button");
    }

   @FXML
   public void passwordButtonAction(){
        //TODO show the password
       if(passwordButton.getText().equals("Show Password")) {
           passwordButton.setText("Hide Password");
           passwordTextField.setText(passwordField.getText());
           passwordTextField.setVisible(true);
           passwordField.setVisible(false);
       }
       else {
           passwordButton.setText("Show Password");
           passwordField.setText(passwordTextField.getText());
           passwordField.setVisible(true);
           passwordTextField.setVisible(false);
       }
    }

    @FXML
    public void addBtnAction( ) {

        resetErrorLabels();
        String errorMessage = "";
        boolean error = false;
        //TODO : create error label beside every text field
        if (nameField.getText().equals("")) {
            errorMessage = "Name Field Cannot be empty!";
            nameSideLabel.setText(errorMessage);
            nameSideLabel.setStyle(" -fx-font-weight: bold");
            error = true;
        } else if (emailAddressField.getText().equals("")) {
            errorMessage = "email Address field cannot be empty!";
            emailSideLabel.setText(errorMessage);
            emailSideLabel.setStyle(" -fx-font-weight: bold");
            error = true;
        }
        else if (passwordField.getText().equals("")) {
            errorMessage = "Password field cannot be empty!";
            passwordSideLabel.setText(errorMessage);
            passwordSideLabel.setStyle(" -fx-font-weight: bold");
            error = true;
        }
        else if(!emailAddressField.getText().contains("@") || emailAddressField.getText().endsWith("@")){
            errorMessage = "please enter a valid email address";
            emailSideLabel.setText(errorMessage);
            emailSideLabel.setStyle(" -fx-font-weight: bold");
            error = true;
        }
        else if (getModelAccess().getEmailAccountNames().contains(emailAddressField.getText())) {
            error = true;
            errorMessage = "This address is used already";
            System.out.println("Used before");
        }
        if (error) {
            AlertBox.display("error",errorMessage);
        }
        else {
            CreateAndRegisterEmailAccountService createAndRegisterEmailAccountService2 =
                    new CreateAndRegisterEmailAccountService(emailAddressField.getText(),
                            passwordField.getText(),
                            ModelAccess.getMainController().getRoot(),
                            getModelAccess());
            createAndRegisterEmailAccountService2.start();
            createAndRegisterEmailAccountService2.setOnSucceeded(event -> {
                Scene messageScene = null;

                if (createAndRegisterEmailAccountService2.getValue() == EmailConstants.LOGIN_STATE_SUCCEEDED) {
                    //TODO: show a label demonstrates that the connection has been done successfully
                    //messageLabel.setText("Successfully connected!");
                    System.out.println("Successfully connected!");

                    //TODO : POP3 or IMAP

                    if(popRadioBtn.isSelected()) {
                        ProtocolRequirementsService emailRetrievalService =
                                new POP3DownloadService(getModelAccess().getEmailAccountsByName(emailAddressField.getText()), false);
                        emailRetrievalService.start();
                    }
                    else {
                        ProtocolRequirementsService emailRetrievalService =
                                new IMAPRetrievalService(getModelAccess().getEmailAccountsByName(emailAddressField.getText()) ,getModelAccess(), ModelAccess.getMainController().getRoot() );
                        emailRetrievalService.start();
                    }
                    //Add to accounts file
                    if(rememberPasswordCheckBox.isSelected()) {
                        App.addAccountToFile(emailAddressField.getText(), passwordField.getText());
                    }
                    //Update UserInfo file
                    App.updateUserInfo(1);

                    //Close the current window
                    Stage stage = (Stage) addBtn.getScene().getWindow();
                    stage.close();

                    //Show message
                    AlertBox.display("Done", "successfully connected to " + emailAddressField.getText());

                }
                 else if (createAndRegisterEmailAccountService2.getValue() == EmailConstants.LOGIN_STATE_FAILED_BY_CREDENTIALS){

                    //TODO: failed to make the connection (maybe the password is not correct)
                    messageLabel.setText("Failed to make the connection");
                    System.out.println("Failed to make the connection");
                    AlertBox.display("Error","Failed to make the connection");
                }

            });
        }

    }

    private void resetErrorLabels() {
        nameSideLabel.setText("Your name, as shown to others");
        nameSideLabel.setStyle("");
        emailSideLabel.setText("Your existing email address");
        emailSideLabel.setStyle("");
        passwordSideLabel.setText("");

    }


    public AddANewAccountController(ModelAccess modelAccess) {
        super(modelAccess);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {
        addBtn.setDefaultButton(true);
        passwordTextField.setVisible(false);

        nameField.setText("rasoul");
        emailAddressField.setText("emailc322@gmail.com");
        passwordField.setText("email.java@client!hello");

        ToggleGroup group = new ToggleGroup();

        popRadioBtn.setToggleGroup(group);
        popRadioBtn.setSelected(true);

        imapRadioBtn.setToggleGroup(group);
        imapRadioBtn.setSelected(false);


        System.out.println("AddANewAccountController has been initialized ");
    }
}
