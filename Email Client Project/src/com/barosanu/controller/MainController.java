package com.barosanu.controller;

import com.barosanu.App;
import com.barosanu.model.EmailMessageBean;
import com.barosanu.model.folder.EmailFolderBean;
import com.barosanu.model.table.BoldableRowFactory;
import com.barosanu.model.table.FormatableInteger;
import com.barosanu.services.CreateAndRegisterEmailAccountService;
import com.barosanu.services.SaveAttachmentsService;
import com.barosanu.view.StyleConstants;
import com.barosanu.view.ViewFactory;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.*;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.DragEvent;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import java.io.*;
import java.net.URL;
import java.util.ArrayList;
import java.util.Date;
import java.util.ResourceBundle;


public class MainController extends AbstractController implements Initializable{


	private boolean updateServices = false;

	private SaveAttachmentsService saveAttachmentsService;

	private MenuItem openInANewWindow = new MenuItem("Open in a new window");

	private EmailFolderBean<String> root;


	@FXML
	private ImageView avatar;

	@FXML
	private Label userInfoLabel;

	@FXML
	private TableColumn<EmailMessageBean, String> statusCol;

	@FXML
	private Button reloadBtn;

	@FXML
	private Button composeButton;

	@FXML
	private Button addANewAccountBtn;

	@FXML
	private Button darkModeBtn;

	@FXML
	private TreeView<String> emailFoldersTreeView ;

	@FXML
	private TableView<EmailMessageBean> emailTableView;

		/* NOT SURE OF THIS EXPLANATION
    <? ?> :
    the first ? : represents the placeholder (the data obj for the row)
    			  they are custom-made obj to represent data
    the second ? : represents value data type.. (ex : string)
     */
		@FXML
		private TableColumn<EmailMessageBean , String> subjectCol;

	@FXML
	private TableColumn<EmailMessageBean, String> senderCol;

	@FXML
	private TableColumn<EmailMessageBean, FormatableInteger> sizeCol;

	@FXML
	private TableColumn<EmailMessageBean, String> dateCol;

	@FXML
	void avatarAction( ) {
		Scene scene = ViewFactory.defaultFactory.getConfigureUserInfoScene();
		Stage stage = new Stage( );
		stage.setOnHidden(event -> reloadUserInfo());

		stage.setScene(scene);
		ViewFactory.addWindow(stage);
		stage.show();

	}

	@FXML
    void composeAction(ActionEvent event) {
		//check of user accounts
		//TODO
		if(getModelAccess().getEmailAccountNames().size() > 0) {
			System.out.println("yes");
			Scene scene = ViewFactory.defaultFactory.getComposeMessageScene();
			Stage stage = new Stage();
			stage.setScene(scene);
			ViewFactory.addWindow(stage);
			stage.show();
		}
		else {
			System.out.println("no");
			/*Scene scene = ViewFactory.defaultFactory.getErrorScene("You haven't registered any account yet");
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();*/
			if(ConfirmBox.display("warning", "You haven't registered any account yet\n" +
					"Do you want to sign in into a new account?")){
				addANewAccount();
			}
		}
	}

	@FXML
	void addANewAccount() {
		Scene scene = ViewFactory.defaultFactory.getAddNewAccountScene();
		Stage stage = new Stage();
		stage.setScene(scene);
		ViewFactory.addWindow(stage);
		stage.setOnCloseRequest(event1 -> ViewFactory.removeWindow(stage));
		stage.show();

		/*if(getModelAccess().getEmailAccountNames().size()>0 && !updateServices){
			//FolderUpdaterService folderUpdaterService = new FolderUpdaterService(getModelAccess().getFoldersList());
			folderUpdaterService.start();
			updateServices = true;
		}*/

	}

	@FXML
	void darkModeAction(){

		if(darkModeBtn.getText().equals("Dark Mood")){
			ViewFactory.defaultFactory.setTheme(StyleConstants.DARK_CSS);
			darkModeBtn.setText("Normal Mood");
		}
		else {
			ViewFactory.defaultFactory.setTheme(StyleConstants.DEFAULT_CSS);
			darkModeBtn.setText("Dark Mood");
		}
	}

	@FXML
	void AddNewAccountMouseEntered( ) {
		System.out.println("entered");
		addANewAccountBtn.setText("Add New Account");
		addANewAccountBtn.setPrefWidth(151);
	}

	@FXML
	void AddNewAccountMouseExited( ) {
		System.out.println("exited");
		addANewAccountBtn.setText("+");
		addANewAccountBtn.setPrefWidth(25);
	}

	@FXML
	void addNewAccountOnDragDetectedAction(MouseEvent event) {
		System.out.println("drag started");
		addANewAccountBtn.setLayoutX(event.getX());
		addANewAccountBtn.setLayoutY(event.getY());
		System.out.println(addANewAccountBtn.getLayoutX());

	}

	@FXML
	void addNewAccountOnDragDoneAction(DragEvent event) {
		addANewAccountBtn.setLayoutX(event.getSceneX());
		addANewAccountBtn.setLayoutY(event.getSceneY());
	}

	@FXML
	void composeButtonMouseEntered( ) {
		composeButton.setText("Compose A New Message");
		composeButton.setPrefWidth(241);
	}

	@FXML
	void composeButtonMouseExited( ) {
		composeButton.setText("+");
		composeButton.setPrefWidth(47);
	}

	@FXML
	void reloadBtn(ActionEvent event) {
		if(App.userHasAccounts() > -1) {
			ArrayList<String> failedUsername = getModelAccess().getFailedUsername();
			for (String username : failedUsername) {
				getModelAccess().getPassword(username);
				CreateAndRegisterEmailAccountService createAndRegisterEmailAccountService2 =
						new CreateAndRegisterEmailAccountService(username,
								getModelAccess().getPassword(username),
								root,
								getModelAccess());
				createAndRegisterEmailAccountService2.start();

			}
		}
	}

	public MainController(ModelAccess modelAccess) {
		super(modelAccess);
	}

	@Override
	public void initialize(URL location, ResourceBundle resources) {

		ViewFactory viewFactory = ViewFactory.defaultFactory;

		emailTableView.setRowFactory(e -> new BoldableRowFactory<>());

		/////////////
		senderCol.setCellValueFactory(new PropertyValueFactory<EmailMessageBean, String>("sender"));
		subjectCol.setCellValueFactory(new PropertyValueFactory<EmailMessageBean, String>("subject"));
		dateCol.setCellValueFactory(new PropertyValueFactory<EmailMessageBean, String>("date"));
		sizeCol.setCellValueFactory(new PropertyValueFactory<EmailMessageBean, FormatableInteger>("size"));
		statusCol.setCellValueFactory(new PropertyValueFactory<EmailMessageBean, String>("seen"));
		sizeCol.setComparator(new FormatableInteger(0));
		////////////

		//this will be hidden
		root = new EmailFolderBean<>("");
		emailFoldersTreeView.setRoot(root);
		emailFoldersTreeView.setShowRoot(false);

		//TODO : this function has to return a boolean value
		if (App.userHasAccounts() > -1) {
			setUpAccounts();
		}

		emailTableView.setContextMenu(new ContextMenu(openInANewWindow));

		emailFoldersTreeView.setOnMouseClicked(event -> {
			System.out.println("Folder Clicked");
			EmailFolderBean<String> item = (EmailFolderBean<String>) emailFoldersTreeView.getSelectionModel().getSelectedItem();
			if (item != null && !item.isTopElement()) {
				emailTableView.setItems(item.getData());
				getModelAccess().setSelectedFolder(item);
				//clear the selected message :
				getModelAccess().setSelectedMessage(null);
			}
		});

		emailTableView.setOnMouseClicked(event -> {
			EmailMessageBean message = emailTableView.getSelectionModel().getSelectedItem();
			if (message != null) {
				getModelAccess().setSelectedMessage(message);

				if (event.getClickCount() == 2) {
				/*	getModelAccess().setSelectedMessage(message);
					Scene scene = ViewFactory.defaultFactory.getEmailDetailsScene();
					Stage stage = new Stage();
					stage.setScene(scene);
					ViewFactory.addWindow(stage);
					stage.show();*/

					TabPane tabPane = (TabPane) addANewAccountBtn.getScene().getRoot();
					String tabId = message.getMessageReference().toString();
					if (ViewFactory.defaultFactory.tabIsOpened(tabId)) {
						Tab tab;
						ObservableList<Tab> tabs = tabPane.getTabs();
						for (Tab temp : tabs) {
							if (temp.getId() != null && tabId.contains(temp.getId())) {
								tabPane.getSelectionModel().select(temp);
								return;
							}
						}
					} else {
						Scene scene = ViewFactory.defaultFactory.getEmailDetailsScene();
						Tab tab = new Tab(message.getSubject());
						AnchorPane content = (AnchorPane) scene.getRoot();
						AnchorPane anchorPane = new AnchorPane();
						anchorPane.getChildren().add(content);
						tab.setContent(scene.getRoot());

						/****/
						tab.setId(tabId);
						/****/
						tab.setOnCloseRequest(event1 -> ViewFactory.defaultFactory.removeOpenedTab(tabId));

						System.out.println(tab.getId());

						ViewFactory.defaultFactory.addOpenedTab(tabId);
						tabPane.getTabs().add(tab);
						tabPane.getSelectionModel().select(tab);
					}
				}
			}
		});

		openInANewWindow.setOnAction(event -> {
			Scene scene = viewFactory.getEmailDetailsScene();
			Stage stage = new Stage();
			stage.setScene(scene);
			stage.show();
		});


		//TODO : #storing
		File file = new File("userInfo.txt");
		try {
			BufferedReader reader = new BufferedReader(new FileReader(file));
			reader.readLine();
			String temp = reader.readLine() + " " + reader.readLine();
			userInfoLabel.setText(temp);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		avatar.setImage(new Image(StyleConstants.AVATAR_IMAGE));


	}

	private void setUpAccounts() {
		//TODO : Read user's account info #storing
		/////////////////////////////////
		File file = new File("userAccounts.txt");
		try {
			if(file.exists()) {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				String emailAddress;
				String password;
				emailAddress = reader.readLine();
				while (emailAddress != null){
						password = reader.readLine();
						System.out.println("[" + emailAddress + "." + password + ")");
						CreateAndRegisterEmailAccountService createAndRegisterEmailAccountService2 =
								new CreateAndRegisterEmailAccountService(emailAddress,
										password,
										root,
										getModelAccess());
					 	createAndRegisterEmailAccountService2.start();



						emailAddress = reader.readLine();
				}
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
		/////////////////////////////////
	}

	private void reloadUserInfo() {
		userInfoLabel.setText(App.userFirstName + " " + App.userLastName);
		avatar.setImage(new Image(StyleConstants.AVATAR_IMAGE));
	}

	public EmailFolderBean<String> getRoot() {
		return root;
	}
}
