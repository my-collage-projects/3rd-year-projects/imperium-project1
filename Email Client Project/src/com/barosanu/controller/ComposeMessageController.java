package com.barosanu.controller;

import com.barosanu.model.EmailConstants;
import com.barosanu.services.SMTPSendingService;
import com.barosanu.services.ProtocolRequirementsService;
import com.barosanu.view.ViewFactory;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.web.HTMLEditor;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.ResourceBundle;

public class ComposeMessageController extends AbstractController implements Initializable {

    private List<File> attachments = new ArrayList<File>();

    private SMTPSendingService emailSenderService;

    @FXML
    private Label attachmentsLabel;

    @FXML
    private ChoiceBox<String> senderChoice;

    @FXML
    private TextField recipientField;

    @FXML
    private TextField subjectField;

    @FXML
    private Label errorLabel;
    @FXML
    private HTMLEditor composeArea;

    @FXML
    private Button sendBtn;

    public ComposeMessageController(ModelAccess modelAccess) {
        super(modelAccess);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {
        senderChoice.setItems(getModelAccess().getEmailAccountNames());
        senderChoice.setValue(getModelAccess().getEmailAccountNames().get(0));

    }

    @FXML
    void attachBtnAction( ) {
        FileChooser fileChooser = new FileChooser();
        File selectedFile = fileChooser.showOpenDialog(null);
        if(selectedFile != null){
            attachments.add(selectedFile);
            attachmentsLabel.setText(attachmentsLabel.getText() + selectedFile.getName() + "; ");
        }

    }

    @FXML
    void sendBtnAction( ) {
        errorLabel.setText("");
        emailSenderService =
                new SMTPSendingService(getModelAccess().getEmailAccountsByName(senderChoice.getValue()),
                        subjectField.getText(),
                        recipientField.getText(),
                        composeArea.getHtmlText());
        emailSenderService.restart();
        emailSenderService.setOnSucceeded(event -> {
            if(emailSenderService.getValue() == EmailConstants.MESSAGE_SENT_OK){
                //errorLabel.setText("message sent successfully");
                /*ViewFactory.defaultFactory.getErrorScene("message sent successfully");
                Stage stage = (Stage) sendBtn.getScene().getWindow();
                stage.close();*/
                AlertBox.display("Congrats", "Successfully sent the message");
            }
            else {
                AlertBox.display("error","Message not sent");
                errorLabel.setText("message sending error!!");
            }
        }); }



}
