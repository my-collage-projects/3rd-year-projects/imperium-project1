package com.barosanu.controller;

import com.barosanu.model.EmailMessageBean;
import com.barosanu.model.folder.EmailFolderBean;
import com.barosanu.view.ViewFactory;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;
import javafx.scene.control.ProgressIndicator;
import javafx.scene.web.WebView;
import javafx.stage.Stage;

import javax.naming.OperationNotSupportedException;
import java.net.URL;
import java.util.ResourceBundle;

public class EmailDetailsController extends AbstractController implements Initializable {

    @FXML
    private WebView webView;

    @FXML
    private Label subjectLabel;

    @FXML
    private Label senderLabel;

    @FXML
    private Button downloadAttachmentBtn;

    @FXML
    private Label downloadingLabel;

    @FXML
    private ProgressBar downloadingProgressBar;

    @FXML
    private ProgressIndicator downloadingProgress2;

    @FXML
    void illegalOperationAction() throws OperationNotSupportedException {
        ViewFactory viewFactory = new ViewFactory();
        Scene mainScene = viewFactory.getMainScene();
        Stage stage = new Stage();
        stage.setScene(mainScene);
        stage.show();
    }

    @FXML
    void downloadAttachmentBtnAction() {
        System.out.println("downAttachBtnAction");
        EmailMessageBean message = getModelAccess().getSelectedMessage();
       /* System.out.println(message.getAttachmentsList().size());
        if (message != null && message.hasAttachments()) {
            System.out.println("Started");


        }*/
    }


    //TODO we don't need this anymore
/*
	@FXML
	void changeReadAction(ActionEvent event) {
		EmailMessageBean message = getModelAccess().getSelectedMessage();
		if(message != null){
			boolean value = message.isRead();
			message.setRead(!value);
			EmailFolderBean<String> selectedFolder = getModelAccess().getSelectedFolder();
			if(selectedFolder != null){
				if(value){
					selectedFolder.incrementUnreadMessagesCount(1);
				}
				else {
					selectedFolder.decrementUnreadMessagesCount();
				}
			}
		}
	}
*/



    @FXML
    void downloadAttachmentBtnAction(ActionEvent event) {
        EmailMessageBean message = getModelAccess().getSelectedMessage();
        if(message != null){
            boolean value = message.isRead();
            message.setRead(!value);
            EmailFolderBean<String> selectedFolder = getModelAccess().getSelectedFolder();
            if(selectedFolder != null){
                if(value){
                    //selectedFolder.incrementUnreadMessagesCount(1);
                }
                else {
                    //selectedFolder.decrementUnreadMessagesCount();
                }
            }
        }
    }


    public EmailDetailsController(ModelAccess modelAccess) {
        super(modelAccess);
    }


    @Override
    public void initialize(URL location, ResourceBundle resources) {

        downloadingProgressBar.setVisible(false);
        downloadingLabel.setVisible(false);

        downloadAttachmentBtn.setDisable(true);

        EmailMessageBean selectedMessage = getModelAccess().getSelectedMessage();

        subjectLabel.setText("Subject: " + selectedMessage.getSubject());
        senderLabel.setText("Sender: " + selectedMessage.getSender());

        webView.getEngine().loadContent(selectedMessage.getMessageReference().getContentAsHTML());

  /*      MessageRendererService messageRendererService = new MessageRendererService(webView.getEngine());
        messageRendererService.setMessageToRender(getModelAccess().getSelectedMessage());
        messageRendererService.restart();
*/
     /*   messageRendererService.setOnSucceeded(event -> {
            *//* if(selectedMessage.hasAttachments()){
                downloadAttachmentBtn.setDisable(false);
            }*//*
            SaveAttachmentsService saveAttachmentsService = new SaveAttachmentsService(downloadingProgressBar, downloadingLabel);
            downloadingProgressBar.progressProperty().bind(saveAttachmentsService.progressProperty());

            saveAttachmentsService.setMessageToDownload(selectedMessage);
            saveAttachmentsService.restart();
        });*/


    }
}
