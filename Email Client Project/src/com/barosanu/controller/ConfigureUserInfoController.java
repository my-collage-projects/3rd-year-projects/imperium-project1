package com.barosanu.controller;

import com.barosanu.App;
import com.barosanu.view.StyleConstants;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.*;
import java.net.URL;
import java.util.ResourceBundle;

public class ConfigureUserInfoController extends AbstractController implements Initializable {

    private String imagePath;

    @FXML
    private ImageView avatarImageView;

    @FXML
    private Button addImage;



    @FXML
    private TextField firstNameField;

    @FXML
    private TextField lastNameField;

    @FXML
    private Button doneBtn;

    @FXML
    void doneHandler(ActionEvent event) {
        /**
         * TODO:
         * We will do some validation here
         */
        boolean error = false;
        String errorMessage = "";
        String firstName = firstNameField.getText();
        String lastName = lastNameField.getText();

        System.out.println("(" +firstName +") (" + lastName +")");

        if (firstNameField.getText().length() ==0) {
            System.out.println("first Name invalid");
            errorMessage += "You must enter First Name";
            error = true;
        }
        if (lastNameField.getText().length() == 0) {
            System.out.println("second Name invalid");
            errorMessage += "\nYou must enter Last Name";
            error = true;
        }
        if (error) {
            /*Scene scene = viewFactory.getErrorScene(errorMessage);
            Stage stage = new Stage();
            stage.setScene(scene);
            stage.show();*/
            AlertBox.display("Missing fields" , "Please fill in all the fields to enter!");

        }
        else {
            //TODO : Save User info to a file #storing
            /////////////////////////////////
            try {
                File outputFile = new File("userInfo.txt");
                if (!outputFile.exists()) {
                    outputFile.createNewFile();
                }
                PrintWriter writer = new PrintWriter(new FileOutputStream("userInfo.txt"));
                writer.println(0);
                writer.println(firstName);
                writer.println(lastName);
                writer.flush();
                writer.close();
                System.out.println("successfully saved configurations");

                App.userFirstName = firstName;
                App.userLastName = lastName;

                //TODO we can add a confirmation alert

                //Close the window
                Stage stage = (Stage) doneBtn.getScene().getWindow();
                stage.close();

               // AlertBox.display("Successfully entered", "Welcome " + firstName + "!");

                App.startMainView();

            } catch (FileNotFoundException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            /////////////////////////////////
        }
    }


    @FXML
    void addImageAction( ) {
        FileChooser fileChooser = new FileChooser();
        fileChooser.setTitle("Open Resource File");
        fileChooser.getExtensionFilters().addAll(
                new FileChooser.ExtensionFilter("Image Files", "*.png", "*.jpg", "*.gif"));
        File selectedFile = fileChooser.showOpenDialog(addImage.getScene().getWindow());
        if (selectedFile != null) {
            imagePath = selectedFile.getPath();
            System.out.println(imagePath);

            try {
                BufferedImage cp, img;
                img = ImageIO.read(selectedFile);
                cp = com.barosanu.utility.Image.deepCopy(img,"avatar");
                Image avatar = new Image(StyleConstants.AVATAR_IMAGE);
                avatarImageView.setFitWidth(150);
                avatarImageView.setFitHeight(150);
                avatarImageView.minHeight(150);
                avatarImageView.minHeight(150);
                avatarImageView.setImage(avatar);

            } catch (IOException e) {
                e.printStackTrace();
            }

        }

    }

    public ConfigureUserInfoController(ModelAccess modelAccess) {
        super(modelAccess);
    }

    @Override
    public void initialize(URL location, ResourceBundle resources) {

        firstNameField.setText(App.userFirstName);
        lastNameField.setText(App.userLastName);

        System.out.println(App.userFirstName);
        System.out.println(App.userLastName);
        avatarImageView.setImage(new Image(StyleConstants.AVATAR_IMAGE));
    }
}
