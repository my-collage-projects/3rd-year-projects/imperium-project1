package com.barosanu.model.folder;

import com.barosanu.model.EmailMessageBean;
import com.barosanu.model.Message;
import com.barosanu.view.ViewFactory;
import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.scene.control.TreeItem;

//import javax.mail.Flags;
//import javax.mail.Message;
//import javax.mail.MessagingException;

public class EmailFolderBean <T> extends TreeItem<String> {

    //This wil indicate that our EmailFolderBean is the top element (example@yahoo.com)
    private boolean topElement = false;
    private int unreadMessageCount;
    private String name;
    @SuppressWarnings("unused")
    private String completeName;
    //For example
    //name = subItem1
    //completeName = sent/subItem1
    private ObservableList<EmailMessageBean> data = FXCollections.observableArrayList();

    /**
     * Constructor for top elements
     * @param value
     */

    public EmailFolderBean(String value) {
        super(value , ViewFactory.defaultFactory.resolveIcon(value));
        this.name = value;
        this.completeName = value;
        data = null;//because the top elem doesn't have any content
        topElement = true;
        this.setExpanded(true);
    }

    /**
     * Constructor for other elements
     * @param value
     * @param completeName
     */

    public EmailFolderBean(String value , String completeName){
        super(value , ViewFactory.defaultFactory.resolveIcon(value));
        this.name = value;
        this.completeName = completeName;

    }

   /* private void updateValue(){
        if(unreadMessageCount > 0){
            this.setValue((String)( name +" (" + unreadMessageCount + ")"));
        }
        else {
            this.setValue(name);
        }
    }

    public void incrementUnreadMessagesCount(int newMessages){
        unreadMessageCount += newMessages;
        updateValue();
    }

    public void decrementUnreadMessagesCount(){
        unreadMessageCount--;
        updateValue();
    }*/

    public void addEmail(int position , Message message) {
        //boolean isRead = message.getFlags().contains(Flags.Flag.SEEN);
        boolean isRead = true;
        EmailMessageBean emailMessageBean = new EmailMessageBean(
                message.getSubject(),
                message.getFrom(),
                message.getSize(),
                isRead,
                message.getDate(), // TODO : #Important
                message);
        if(position < 0) {
            data.add(emailMessageBean);
        }
        else {
            data.add(position,emailMessageBean);
        }
        if(!isRead){
            //TODO
            //incrementUnreadMessagesCount(1);
        }
    }

    public boolean isTopElement(){
        return topElement;
    }

    public ObservableList<EmailMessageBean> getData(){

        for(EmailMessageBean x : data){
            System.out.println(x.toString());
            System.out.println("=======================");
        }

        return data;
    }

}
