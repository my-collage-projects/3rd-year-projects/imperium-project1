package com.barosanu.model.folder;

import com.barosanu.model.Message;

import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Cezar
 */
public class Folder {

    public static enum FolderType {
        INBOX,
        SENT,
        ALL_MAILS;
    }
    
    FolderType folderType;
    List<Message> messages = new ArrayList<Message>();
    
    public int getMessageCount() {
        return messages.size();
    }

    public Message getMessage(int i) {
        return messages.get(i);
    }
    
    public Folder(FolderType type) {
        this.folderType = type;
    }
    
    public String getName() {
        return folderType.toString();
    }
    
    public String getFullName() {
        return "GMAIL/" + getName();
    }
    
    public void addMessage(Message msg) {
        messages.add(msg);
    }
    public FolderType getType() {
        return folderType;
    }
    
}
