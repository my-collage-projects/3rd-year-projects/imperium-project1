package com.barosanu.model;

import javafx.util.Pair;

import javax.net.ssl.SSLSocket;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedReader;
import java.io.PrintWriter;

/**
 * @Author Raghad Hanna
 *
 * This Class Is Responsible For Establishing A Secure TCP Connection Between
 * The Client (This Computer) & Some Server & Closing It
 *
 * This Class Doesn't Hardcode The Server To Connect To Or The Port On Which
 * The Server Listens For Requests, Because A Connection Could Be Established For Many
 * Purposes (Send, Retrieve, Login ..), That Way, It Can Serve In A Lot Of Places
 */

public class TCPConnection {
    // The Socket Through Which The Connection Is Going To Be Established
    static SSLSocket socket = null;

    // An Enumeration That Helps The Outside World Determine Which Protocol
    // To Use When Calling This Service

    public static SSLSocket establishTCPConnection(String senderAddress, Protocols protocol) throws Exception {
        // Getting The SSLSocketFactory For Creating An SSLSocket With It
        SSLSocketFactory factory = (SSLSocketFactory) SSLSocketFactory.getDefault();

        // Getting The The Server I Need To Connect To & The Port On Which The
        // Server Is Listening On By Calling Another Service (MailServersFactory)
        Pair<String, Integer> arguments = null;
        switch(protocol) {
            case SMTP:
                arguments = MailServersFactory.getSMTPServerByDomain(senderAddress);
                break;
            case POP3:
                arguments = MailServersFactory.getPOP3ServerByDomain(senderAddress);
                break;
            case IMAP:
                arguments = MailServersFactory.getIMAPServerByDomain(senderAddress);
                break;
        }

        // Establishing The Connection With The Provided Connection Requirements
        socket = (SSLSocket) factory.createSocket(arguments.getKey(),
                arguments.getValue());

        // Validating The Authenticity Of The Server, Exchanging Keys Between
        // The End Hosts & Specifying The Encryption Algorithm Used
        // Basically, Agreeing On Some Rules To Follow To Achieve Security & Privacy
        socket.startHandshake();
        return socket;
    }

    // Close The Socket And The Input & Output Streams Between The End Hosts
    public static void closeTCPConnection(SSLSocket socket, BufferedReader in, PrintWriter out) throws Exception {
        in.close();
        out.close();
        socket.close();
    }
}
