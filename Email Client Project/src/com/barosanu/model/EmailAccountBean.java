package com.barosanu.model;

import com.barosanu.model.folder.Folder;

import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.Base64;
import java.util.List;


public class EmailAccountBean {

    private Protocols selectedProtocol;

    private String emailAddress;
    private String password;

    private int loginState = EmailConstants.LOGIN_STATE_NOT_READY;

    private SSLSocket socket;

    private PrintWriter out;
    private BufferedReader in;

    //I need a list of folders
    private List<Folder> folders = new ArrayList<Folder>();
    public List<Folder> getFolders() {
        return folders;
    }

    public EmailAccountBean(String email , String password){
        this.emailAddress = email;
        this.password = password;
        //folders[0] = new Folder(Folder.FolderType.INBOX);
        //folders[1] = new Folder(Folder.FolderType.SENT);
        folders.add(new Folder(Folder.FolderType.INBOX));
        folders.add(new Folder(Folder.FolderType.SENT));
        folders.add(new Folder(Folder.FolderType.ALL_MAILS));
    }

    public void initializeSocket() throws Exception {
        socket = TCPConnection.establishTCPConnection(this.emailAddress, Protocols.SMTP);
    }

    public void initializeStreams() throws Exception {
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public boolean authenticateLogin() {
        try {
            initializeSocket();
            initializeStreams();
            System.out.println(serverResponse());

            out.println("HELO host");
            String response = serverResponse();
            System.out.println(response);

            out.println("AUTH LOGIN");
            response = serverResponse();
            System.out.println(response);

            out.println(Base64.getEncoder().encodeToString(this.emailAddress.getBytes()));
            response = serverResponse();
            System.out.println(response);

            out.println(Base64.getEncoder().encodeToString(this.password.getBytes()));
            response = serverResponse();
            System.out.println(response);

            out.println("QUIT");

            if(!response.contains("235 ")) {
                closeConnection();
                this.loginState = EmailConstants.LOGIN_STATE_FAILED_BY_CREDENTIALS;
                return false;
            }
            else {
                closeConnection();
                this.loginState = EmailConstants.LOGIN_STATE_SUCCEEDED;
                return true;
            }

        } catch(Exception e) {
            //System.err.println(e.toString());
            e.printStackTrace();
            this.loginState = EmailConstants.LOGIN_STATE_FAILED_BY_NETWORK;
            return false;
        }
    }

    //We will disguss it later
    /*public boolean authenticateLoginByIMAP() {
        try {
            initializeSocket();
            initializeStreams();
            System.out.println(serverResponse());
            out.println("a LOGIN " + this.emailAddress + " " + password);
            String response = serverResponse();
            response = response.toLowerCase();
            if(response.contains("failure") || response.contains("failed")) {
                closeConnection();
                this.loginState = EmailConstants.LOGIN_STATE_FAILED_BY_CREDENTIALS;
                return false;
            }
            else {
                closeConnection();
                this.loginState = EmailConstants.LOGIN_STATE_SUCCEEDED;
                return true;
            }
        } catch(Exception e) {
            //System.err.println(e.toString());
            e.printStackTrace();
            this.loginState = EmailConstants.LOGIN_STATE_FAILED_BY_NETWORK;
            return false;
        }
    }*/

    public void closeConnection() throws Exception {
        socket.close();
        in.close();
        out.close();
    }

    public String serverResponse() throws Exception {
        return in.readLine();
    }

    public String getEmailAddress() {
        return this.emailAddress;
    }

    public int getLoginState() {
        return this.loginState;
    }

    public String getPassword() {
        return this.password;
    }

    public void setSelectedProtocol(Protocols protocol) {
        this.selectedProtocol = protocol;
    }
    public Protocols getSelectedProtocol() {
        return this.selectedProtocol;
    }

}
