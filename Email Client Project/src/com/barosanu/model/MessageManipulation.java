package com.barosanu.model;

/**
 * @Author Raghad Hanna
 *
 * Utility Class, All The Methods Are Static
 * It Manipulates The Original Content Of The Message Received From The POP3 Server
 * And Gets Us The Necessary Stuff
 */

public class MessageManipulation {
    // ِ Represents The Passed String, Which Contains The
    // Whole Message Received From The POP3 Server
    public static String content;

    public static void setContent(String contentIN) {
        content = contentIN;
    }

    // Represents The Whole Message But In Separate Lines,
    // For Message Manipulation Purposes
    private static String[] lines;

    // The Method That Will Analyze The Original Message Line By Line
    // And Will Extract From It (The Message Attributes),
    // What's Necessary As To What The Outside World Requires
    public static String[] getMessageAttributes() {
        // Represents The Boundary In The Original Message
        StringBuffer boundary = new StringBuffer();

        // Splitting The Original Message By A Line Separator
        lines = content.split("\n");

        // The String Array That Will Be Filled And Returned
        String[] messageAttributes = new String[6];
        int index = 0;

        // A Boolean That Helps In Satisfying The Condition Of Finding A Line
        // That Starts With A "Content-Type" Only Once Because There Are
        // Multiple Lines That Start With This String
        boolean firstContent_Type = true;

        for(int i = 0; i < lines.length; i++) {
            if(lines[i].startsWith("Content-Type") && firstContent_Type) {
                // Get Me The Boundary "?"
                boundary.append(lines[i].split("=")[1]);
                // Delete The " In The Beginning
                boundary.deleteCharAt(0);
                // Delete The " At The End
                boundary.deleteCharAt(boundary.length() - 1);
                // Make Sure We Dont Enter This State Again
                firstContent_Type = false;
            }
            else if(lines[i].contains("From:")
                    || lines[i].startsWith("Date:")
                    || lines[i].startsWith("Message-ID:")
                    || lines[i].startsWith("Subject:")
                    || lines[i].startsWith("To:")) {
                // Get Me The Values From The Previous Lines (The Important Stuff)
                messageAttributes[index] = getValue(lines[i]);
                index++;
            }
            // The Stage Of Reading The Actual Content Represented As HTML
            else if(lines[i].startsWith("--" + boundary) && lines[i+1].startsWith("Content-Type: text/html;")) {
                messageAttributes[index] = getActualContent(boundary, i+3);
                return messageAttributes;
            }
        }
        return messageAttributes;
    }

    // Getting The Actual Content That Is Between The Boundaries
    public static String getActualContent(StringBuffer boundary, int startingIndex) {
        String actualContent = "";
        String stringBoundary = boundary.toString();
        while(!(lines[startingIndex].startsWith("--" + stringBoundary + "--"))) {
            actualContent += lines[startingIndex];
            startingIndex++;
        }
        return actualContent;
    }

    // Getting The Info Needed From The line String
    public static String getValue(String line) {
        return line.split(": ")[1];
    }
}
