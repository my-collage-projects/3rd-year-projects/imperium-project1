package com.barosanu.model;

/**
 *
 * @author Cezar
 */
public class MessageHeader {

    private String mailFrom;
    private String rcptTo;
    private String subject;

    public MessageHeader(String from, String to, String sub) {
        this.mailFrom = from;
        this.rcptTo = to;
        this.subject = sub;
    }

    @Override
    public String toString() {
        return "From: " + mailFrom + "\n" +
                "To: " + rcptTo + "\n" +
                "Subject: " + subject + "\n";
    }

    public String getMailFrom() {
        return mailFrom;
    }

    public String getRcptTo() {
        return rcptTo;
    }

    public String getSubject() {
        return subject;
    }
}
