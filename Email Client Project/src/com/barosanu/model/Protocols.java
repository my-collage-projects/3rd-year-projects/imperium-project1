package com.barosanu.model;

/**
 *
 * @author Cezar
 */
// An Enumeration That Helps The Outside World Determine Which Protocol
// To Use When Calling This Service
public enum Protocols {
    SMTP,
    POP3,
    IMAP,
}
