package com.barosanu.model;

/**
 * @Author Raghad Hanna
 *
 * This Class Represents The Message That Will Be Built
 * Upon The Raw Message Received From The POP3/IMAP Servers
 * By Analyzing The Content And Extracting The Important Stuff
 * And Will Be Encapsulated By A More Formal EmailMessageBean Class
 */

public class Message {
    String from;
    // TODO We Don't Have A Date As An Object Yet
    String date;
    String message_ID;
    String subject;
    String to;
    String contentAsHTML;
    int size;

    // ِ Represents The Passed String, Which Contains The
    // Whole Message Received From The POP3/IMAP Server
    String fullContent;

    // Represents The Whole Message But In Separate Lines,
    // For Message Manipulation Purposes
    private String[] lines;

    public String getFrom() {
        return from;
    }
    public String getDate() {
        return date;
    }
    public String getSubject() {
        return subject;
    }
    public String getTo() { return to; }
    public String getContentAsHTML() {
        return contentAsHTML;
    }
    public int getSize() {
        return size;
    }

    public Message(String fullContent) {
        this.fullContent = fullContent;
        getMessageAttributes();
        //for(int i = 0 ; i < lines.length ; i++) {
        //    System.out.println(lines[i]);
        //}
        System.out.println(getDate());
    }

    // The Method That Will Analyze The Original Message Line By Line
    // And Will Extract From It (The Message Attributes),
    // What's Necessary As To What The Outside World Requires
    public void getMessageAttributes() {
        // Represents The Boundary In The Original Message
        StringBuffer boundary = new StringBuffer();

        // Splitting The Original Message By A Line Separator
        lines = fullContent.split("\n");

        // A Boolean That Helps In Satisfying The Condition Of Finding A Line
        // That Starts With A "Content-Type" Only Once Because There Are
        // Multiple Lines That Start With This String
        boolean firstContent_Type = true;

        for(int i = 0; i < lines.length; i++) {
            if(lines[i].contains("Content-Type") /*&& firstContent_Type*/) {
                /*// Get Me The Boundary "?"
                boundary.append(lines[i].split("=")[1]);
                // Delete The " In The Beginning
                boundary.deleteCharAt(0);
                // Delete The " At The End
                boundary.deleteCharAt(boundary.length() - 1);
                // Make Sure We Don't Enter This State Again
                firstContent_Type = false;*/
                this.contentAsHTML = getActualContent(boundary, i+1);
                this.size = (this.contentAsHTML.length() * 2);

            }
            else if(lines[i].startsWith("From:")) {
                String value = getValue(lines[i]);
                this.from = value;
            }
            else if(lines[i].startsWith("Date:")) {
                String value = getValue(lines[i]);
                this.date = value;
                //dateObject = new Date("")
            }
            else if(lines[i].startsWith("Message-ID:")) {
                String value = getValue(lines[i]);
                this.message_ID = value;
            }
            else if(lines[i].startsWith("Subject:")) {
                String value = getValue(lines[i]);
                this.subject = value;
            }
            else if(lines[i].startsWith("To:")) {
                String value = getValue(lines[i]);
                this.to = value;
            }
            /*
            // The Stage Of Reading The Actual Content Represented As HTML
            //else if(lines[i].startsWith("--" + boundary.toString()) && lines[i+1].startsWith("Content-Type: text/html;")) {
            else if(lines[i].contains("Content-Type: text/plain;")) {
                this.contentAsHTML = getActualContent(boundary, i+3);
                this.size = (this.contentAsHTML.length() * 2);
                return;
            }
            else if(lines[i].contains("Content-Type: text/html;")) {
                this.contentAsHTML = getActualContent(boundary, i+3);
                this.size = (this.contentAsHTML.length() * 2);
                return;
            }*/
        }
    }

    // Getting The Actual Content That Is Between The Boundaries
    public String getActualContent(StringBuffer boundary, int startingIndex) {
        String actualContent = "";
        String stringBoundary = boundary.toString();
        /*while(!(lines[startingIndex].startsWith("--" + stringBoundary + "--"))) {
            actualContent += lines[startingIndex] + "\n";
            startingIndex++;
        }*/
        while(startingIndex < lines.length) {
            actualContent += lines[startingIndex] + "\n";
            startingIndex++;
        }
        return actualContent;
    }

    // Getting The Info Needed From The line String
    public String getValue(String line) {
        return line.split(": ")[1];
    }
}
