package com.barosanu.model;

import com.barosanu.model.table.AbstractTableItem;
import com.barosanu.model.table.FormatableInteger;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;

public class EmailMessageBean extends AbstractTableItem {

/*
    // static : to make it unique to all the project
    public static Map<String , Integer> formattedValues = new HashMap<>();
*/

    private SimpleStringProperty sender;
    private SimpleStringProperty subject;
    private SimpleObjectProperty<FormatableInteger> size;
    private Message messageReference;
    private SimpleStringProperty date;

    //Attachment handling
    // private List<MimeBodyPart> attachmentsList = new ArrayList<>();
    private StringBuffer attachmentsNames = new StringBuffer();

    /*public List<MimeBodyPart> getAttachmentsList() {
        return attachmentsList;
    }

    public String getAttachmentsNames() {
        return attachmentsNames.toString();
    }

    public void addAttachment(MimeBodyPart mbp){
        attachmentsList.add(mbp);
        try {
            attachmentsNames.append(mbp.getFileName() + "; ");
        } catch (MessagingException e) {
            e.printStackTrace();
        }
    }

    public boolean hasAttachments(){
        System.out.println("attachment List" + attachmentsList.toString());
        return attachmentsList.size() > 0;
    }

    //clear method:
    public void clearAttachments(){
        attachmentsList.clear();
        attachmentsNames.setLength(0); // how to clear a StringBuffer
    }*/


    @Override
    public String toString() {
        return "EmailMessageBean{" +
                "sender=" + sender +
                ", subject=" + subject +
                ", size=" + size +
                ", messageReference=" + messageReference +
                '}';
    }

    public EmailMessageBean(String subject, String sender, int size, boolean isRead ,String date, Message messageReference) {
        super(isRead);
        this.sender = new SimpleStringProperty(sender);
        this.subject = new SimpleStringProperty(subject);
        this.size = new SimpleObjectProperty<FormatableInteger>(new FormatableInteger(size));
        this.messageReference = messageReference;
        this.date = new SimpleStringProperty(date);
    }

    public String getDate() {
        return date.get();
    }

    public String getSender() {
        return sender.get();
    }


    public String getSubject() {
        System.out.println(messageReference.getSubject());
        return messageReference.getSubject();
    }


    public FormatableInteger getSize() {
        return size.get();
    }

    public Message getMessageReference() {
        return messageReference;
    }

}
