package com.barosanu.model;

import javafx.util.Pair;

/**
 * @Author Raghad Hanna
 *
 * This Class Is A Factory That Is Responsible For Getting The Right Server
 * & Port Number Depending On What The Outside World Requires (Which Protocol
 * To Use And Therefore Which Port), A Specific Method Will Be Called
 */

public class MailServersFactory {
    // A List Of SMTP, POP3 & IMAP Servers Represented As Constants
    // So They Don't Change Accidentally
    public final static String GMAIL_SMTP = "smtp.gmail.com";
    public final static String YAHOOMAIL_SMTP = "smtp.yahoo.mail.com";
    public final static String OUTLOOK_SMTP = "smtp.office365.com";

    public final static String GMAIL_POP3 = "pop.gmail.com";
    public final static String YAHOOMAIL_POP3 = "pop.mail.yahoo.com";
    public final static String OUTLOOK_POP3 = "outlook.office365.com";

    public final static String GMAIL_IMAP = "imap.gmail.com";
    public final static String YAHOOMAIL_IMAP = "imap.mail.yahoo.com";
    public final static String OUTLOOK_IMAP = "outlook.office365.com";

    // A List Of SMTP, POP3 & IMAP Ports Represented As Constants
    // So THey Don't Change Accidentally
    public final static int SMTP_SSL_PORT = 465;
    public final static int SMTP_STARTTLS_PORT = 587;

    public final static int IMAP_SSL_PORT = 993;
    public final static int POP3_SSL_PORT = 995;

    // Return Me The Requirements (The SMTP Mail Server & The Port On Which
    // The Server Is Listening On) Based On The Domain Name Of The Email Address Passed
    public static Pair<String, Integer> getSMTPServerByDomain(String emailAddress) {
        // A String Representing The Server Name & An Integer Representing The Port
        // The Server Listens On
        Pair<String, Integer> TCPConnectionRequirements = null;

        // Get Me What's After The '@' Sign (Aka, The Domain Name)
        String domainName = emailAddress.split("@")[1];

        switch(domainName) {
            case "gmail.com":
                TCPConnectionRequirements = new Pair<>(GMAIL_SMTP, SMTP_SSL_PORT);
                break;
            case "yahoo.com":
                TCPConnectionRequirements = new Pair<>(YAHOOMAIL_SMTP, SMTP_SSL_PORT);
                break;
            case "outlook.com":
            case "hotmail.com":
                TCPConnectionRequirements = new Pair<>(OUTLOOK_SMTP, SMTP_STARTTLS_PORT);
                break;
        }
        return TCPConnectionRequirements;
    }

    // Return Me The Requirements (The POP3 Mail Server & The Port On Which
    // The Server Is Listening On) Based On The Domain Name Of The Email Address Passed
    public static Pair<String, Integer> getPOP3ServerByDomain(String emailAddress) {
        // A String Representing The Server Name & An Integer Representing The Port
        // The Server Listens On
        Pair<String, Integer> TCPConnectionRequirements = null;

        // Get Me What's After The '@' Sign (Aka, The Domain Name)
        String domainName = emailAddress.split("@")[1];

        switch(domainName) {
            case "gmail.com":
                TCPConnectionRequirements = new Pair<>(GMAIL_POP3, POP3_SSL_PORT);
                break;
            case "yahoo.com":
                TCPConnectionRequirements = new Pair<>(YAHOOMAIL_POP3, POP3_SSL_PORT);
                break;
            case "outlook.com":
            case "hotmail.com":
                TCPConnectionRequirements = new Pair<>(OUTLOOK_POP3, POP3_SSL_PORT);
                break;
        }
        return TCPConnectionRequirements;
    }

    // Return Me The Requirements (The IMAP Mail Server & The Port On Which
    // The Server Is Listening On) Based On The Domain Name Of The Email Address Passed
    public static Pair<String, Integer> getIMAPServerByDomain(String emailAddress) {
        // A String Representing The Server Name & An Integer Representing The Port
        // The Server Listens On
        Pair<String, Integer> TCPConnectionRequirements = null;

        // Get Me What's After The '@' Sign (Aka, The Domain Name)
        String domainName = emailAddress.split("@")[1];

        switch(domainName) {
            case "gmail.com":
                TCPConnectionRequirements = new Pair<>(GMAIL_IMAP, IMAP_SSL_PORT);
                break;
            case "yahoo.com":
                TCPConnectionRequirements = new Pair<>(YAHOOMAIL_IMAP, IMAP_SSL_PORT);
                break;
            case "outlook.com":
            case "hotmail.com":
                TCPConnectionRequirements = new Pair<>(OUTLOOK_IMAP, IMAP_SSL_PORT);
                break;
        }
        return TCPConnectionRequirements;
    }
}
