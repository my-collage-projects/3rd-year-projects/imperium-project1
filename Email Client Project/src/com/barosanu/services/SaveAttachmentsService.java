package com.barosanu.services;

import com.barosanu.model.EmailMessageBean;
import javafx.concurrent.Service;
import javafx.concurrent.Task;
import javafx.scene.control.Label;
import javafx.scene.control.ProgressBar;

//import javax.mail.internet.MimeBodyPart;

public class SaveAttachmentsService extends Service<Void> {

    // here we save the attachment
    private String LOCATION_OF_DOWNLOADS = System.getProperty("user.home") + "/Downloads/Email Client downloads/";


    private EmailMessageBean messageToDownload ;
    //these will start hidden
    private ProgressBar progress;
    private Label label;


    public SaveAttachmentsService(ProgressBar progress, Label label) {
        this.progress = progress;
        this.label = label;
        //we can only update visual component on the main thread
        this.setOnRunning(e->{showVisuals(true);});
        this.setOnSucceeded(e->{showVisuals(false);});
        System.out.println("SaveAttachmentService Constructed!");
    }

    //Always call before starting!!!!
    public void setMessageToDownload(EmailMessageBean messageToDownload) {
        this.messageToDownload = messageToDownload;
    }


    private void showVisuals(boolean show){
        progress.setVisible(show);
        label.setVisible(show);
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>(){
            @Override
            protected Void call() throws Exception {
                /*try {
                    System.out.println(messageToDownload.getAttachmentsList().size() + " size");
                    for(MimeBodyPart mbp: messageToDownload.getAttachmentsList()){
                        System.out.println("File name : " + mbp.getFileName());
                        updateProgress(messageToDownload.getAttachmentsList().indexOf(mbp),
                                messageToDownload.getAttachmentsList().size());
                        mbp.saveFile(LOCATION_OF_DOWNLOADS + mbp.getFileName());
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
                return null;
            }
        };
    }
}
