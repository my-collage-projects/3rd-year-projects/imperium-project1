package com.barosanu.services;

import com.barosanu.model.EmailAccountBean;
import com.barosanu.model.EmailConstants;
import com.barosanu.model.Protocols;
import com.barosanu.model.TCPConnection;
import javafx.concurrent.Service;

import javax.net.ssl.SSLSocket;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;

/**
 * @Authors Raghad And Cezar
 *
 * This Class Defines Common Requirements For Any Protocol
 * To Do Its Job, Like Opening A Connection With A Specified Server
 * And Initializing The Streams For Data Exchange ..
 * Basically, All The Common Code Is Here And So We Avoided Redundant Code
 */

public abstract class ProtocolRequirementsService extends Service<Integer> {
    protected EmailAccountBean userAccount;
    protected SSLSocket socket;
    protected BufferedReader in;
    protected PrintWriter out;
    protected int state;

    public ProtocolRequirementsService(EmailAccountBean userAccount) {
        this.userAccount = userAccount;
        // Retrieving Emails Of An Account Implies That The User Has Already Logged In Successfully
        state = EmailConstants.LOGIN_STATE_SUCCEEDED;
    }

    // Connecting To A Server Through The protocol Passed
    // And Getting The Socket Streams Ready For Communication
    public int establishConnection(Protocols protocol) {
        try {
            // Required Steps Of Any Protocol Implementation
            // 1st Step: Establishing The Connection Between The Client (This Computer)
            // And The Server By Calling The Service TCPConnectionEstablishment
            socket = TCPConnection.establishTCPConnection(userAccount.getEmailAddress(),
                    protocol);

            // 2nd Step: Opening Input & Output Streams Between
            // The Client & The Server For Data Exchange
            // In The 3rd Step (This Step Is Dependent On The Protocol Used
            // And This Class Specifies The General Requirements For Any Protocol
            // And Therefore The Protocol Details Will Be Implemented In The Subclasses)
            initializeStreams();
        }
        catch(Exception e) {
            state = EmailConstants.CONNECTION_FAILED;
        }
        return state;
    }

    // Get Me The Input & Output Streams That The socket Object Provides
    private void initializeStreams() throws Exception {
        out = new PrintWriter(new OutputStreamWriter(socket.getOutputStream()), true);
        in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
    }

    public void writeClientResponse(String clientResponse) {
        out.println(clientResponse);
    }

    public void closeConnection() throws Exception {
            TCPConnection.closeTCPConnection(socket, in, out);
    }
}
