package com.barosanu.services.pop3services;

import com.barosanu.controller.ModelAccess;
import com.barosanu.model.EmailAccountBean;
import com.barosanu.model.folder.EmailFolderBean;
import com.barosanu.model.folder.Folder;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.util.List;

//import javax.mail.Folder;
//import javax.mail.Message;
//import javax.mail.MessagingException;
//import javax.mail.event.MessageCountAdapter;
//import javax.mail.event.MessageCountEvent;

public class FetchFoldersService extends Service<Void> {

    private EmailFolderBean<String> foldersRoot;
    private EmailAccountBean emailAccountBean;
    private ModelAccess modelAccess;
    private static int NUMBER_OF_FETCH_FOLDER_SERVICES_ACTIVE = 0;

    public FetchFoldersService(EmailFolderBean<String> foldersRoot, EmailAccountBean emailAccountBean , ModelAccess modelAccess) {
        this.foldersRoot = foldersRoot;
        this.emailAccountBean = emailAccountBean;
        this.modelAccess = modelAccess;

        this.setOnSucceeded(e->
                NUMBER_OF_FETCH_FOLDER_SERVICES_ACTIVE--
        );
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                NUMBER_OF_FETCH_FOLDER_SERVICES_ACTIVE++ ;
                if(emailAccountBean != null){
                    List<Folder> folders = emailAccountBean.getFolders();
                    for(Folder folder: folders){
                        modelAccess.addFolder(folder);
                        EmailFolderBean<String> item = new EmailFolderBean<String>(folder.getName(),folder.getFullName());
                        foldersRoot.getChildren().add(item);
                        item.setExpanded(true);
                        //addMessageListenerToFolder(folder,item);

                        FetchMessagesOnFolderService fetchMessagesOnFolderService =
                                new FetchMessagesOnFolderService(emailAccountBean.getEmailAddress(), item, folder);
                        fetchMessagesOnFolderService.start();
                        System.out.println("Added: " + folder.getName());
                        //This will get only main folders
                        //so we have to iterate again
                        /*Folder subFolders[] = folder.list();
                        for(Folder subFolder : subFolders){
                            modelAccess.addFolder(subFolder);
                            EmailFolderBean<String> subItem = new EmailFolderBean<String>(subFolder.getName(),subFolder.getFullName());
                            item.getChildren().add(subItem);
                            addMessageListenerToFolder(subFolder,subItem);
                            FetchMessagesOnFolderService fetchMessagesOnSubFolderService =
                                    new FetchMessagesOnFolderService(subItem,subFolder);
                            fetchMessagesOnSubFolderService.start();
                            System.out.println("added: " + subFolder.getName());
                        }*/
                    }
                }
                return null;
            }
        };
    }

    /*private void addMessageListenerToFolder(Folder folder, EmailFolderBean<String> item){
        folder.addMessageCountListener(new MessageCountAdapter() {
            @Override
            public void messagesAdded(MessageCountEvent e) {
                for(int i = 0 ; i  <  e.getMessages().length ; i++){
                    try {
                        Message currentMessage = folder.getMessage(folder.getMessageCount() - i);
                        item.addEmail(0, currentMessage);// it will add it to the start of list
                    } catch (MessagingException messagingException) {
                        messagingException.printStackTrace();
                    }
                }
            }
        });
    }*/

    public static boolean noServicesActive(){
        return NUMBER_OF_FETCH_FOLDER_SERVICES_ACTIVE == 0;
    }

}
