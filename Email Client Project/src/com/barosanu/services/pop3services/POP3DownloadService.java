package com.barosanu.services.pop3services;

import com.barosanu.App;
import com.barosanu.model.EmailAccountBean;
import com.barosanu.model.EmailConstants;
import com.barosanu.model.Protocols;
import com.barosanu.services.ProtocolRequirementsService;
import javafx.concurrent.Task;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author Cezar
 */
public class POP3DownloadService extends ProtocolRequirementsService {

    private int state;

    //true if the user wants to update manually
    //false if he wants to automatically update
    private final boolean retrievalOnUserCommand;

    private final int UPDATE_TIMER = 10000;

    private int numberOfDownloadedEmails = 0;
    private int numberOfEmailsOnServer = 0;

    public POP3DownloadService(EmailAccountBean userAccount, boolean retrievalOnUserCommand) {
        super(userAccount);
        this.retrievalOnUserCommand = retrievalOnUserCommand;
        state = EmailConstants.LOGIN_STATE_SUCCEEDED;

        //Number of downloaded emails in emails folder
        new File("emails\\" + userAccount.getEmailAddress()).mkdirs();
        numberOfDownloadedEmails = (new File("emails\\" + userAccount.getEmailAddress() + "\\").listFiles().length);
    }


    @Override
    protected Task<Integer> createTask() {
        return new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                while(!App.isClosed) {

                    state = establishConnection(Protocols.POP3);
                    if(state == EmailConstants.CONNECTION_FAILED)
                        return state;

                    String serverResponse = readServerResponse();
                    System.out.println(serverResponse);

                    //Send Email Address
                    //writeClientResponse("USER " + userAccount.getEmailAddress());
                    writeClientResponse("USER recent:" + userAccount.getEmailAddress());
                    serverResponse = readServerResponse();
                    System.out.println(serverResponse);

                    //Send Password
                    //writeClientResponse("PASS " + userAccount.getPassword());
                    writeClientResponse("PASS " + userAccount.getPassword());
                    serverResponse = readServerResponse();
                    System.out.println(serverResponse);


                    //Get the number of emails on the server
                    writeClientResponse("STAT");
                    serverResponse = readServerResponse();
                    numberOfEmailsOnServer = Integer.parseInt(serverResponse.split(" ")[1]);
                    System.out.println(serverResponse);

                    //if there are new emails!!
                    while(numberOfDownloadedEmails < numberOfEmailsOnServer) {
                        //Download the next email
                        if(downloadEmailFromServer(numberOfDownloadedEmails + 1)) {
                            //update NumberOfDownloadedEmails
                            App.newFetch = true;
                            numberOfDownloadedEmails++;
                        }
                    }
                    writeClientResponse("QUIT");
                    serverResponse = readServerResponse();
                    System.out.println(serverResponse);

                    closeConnection();

                    //numberOfDownloadedEmails = (new File("emails\\ " + userAccount.getEmailAddress() + "\\").listFiles().length);
                    if(!retrievalOnUserCommand) {
                        try {
                            Thread.sleep(UPDATE_TIMER);
                        } catch (InterruptedException ex) {
                            App.isClosed = true;
                        }
                    }
                    else {
                        break;
                    }
                }

                return state;
            }
        };
    }

    public boolean downloadEmailFromServer(int emailIndex) {
        try {
            //Request the email from the server
            writeClientResponse("RETR " + emailIndex);
            System.out.println("RETR " + emailIndex);
            System.out.println(readServerResponse());
            BufferedWriter fileWriter =
                    new BufferedWriter(new FileWriter(new File("emails\\" + userAccount.getEmailAddress() + "\\" + Integer.toString(emailIndex) + ".txt")));
            String serverResponse;
            while(!(serverResponse = in.readLine()).equals(".")) {
                fileWriter.write(serverResponse);
                fileWriter.newLine();
            }
            fileWriter.close();
        } catch(IOException e) {
            System.out.println("Failed to download E-mail: " + emailIndex);
            return false;
        }
        System.out.println("E-mail: " + emailIndex + " has been downloaded successfully!!");
        return true;
    }

    public String readServerResponse() {
        // TODO Should Fill It With Something That Works For Both Reading
        // TODO Inside A While Loop And For Reading A Line
        String serverResponse = null;
        try {
            serverResponse = in.readLine();
        }
        catch(Exception e) {
        }
        return serverResponse;
    }
}
