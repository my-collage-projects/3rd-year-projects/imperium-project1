package com.barosanu.services.pop3services;

import com.barosanu.App;
import com.barosanu.model.Message;
import com.barosanu.model.folder.EmailFolderBean;
import com.barosanu.model.folder.Folder;
import javafx.concurrent.Service;
import javafx.concurrent.Task;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;

public class FetchMessagesOnFolderService extends Service<Void> {

    private final int TIMER = 10000;

    private EmailFolderBean<String> emailFolder;
    //for every folder we will have a service
    private Folder folder;

    private String emailAddress;

    public FetchMessagesOnFolderService(String eAddress, EmailFolderBean<String> emailFolder, Folder folder) {
        this.emailFolder = emailFolder;
        this.folder = folder;
        this.emailAddress = eAddress;
    }

    @Override
    protected Task<Void> createTask() {
        return new Task<Void>() {
            @Override
            protected Void call() throws Exception {
                int currentMessageIndex = 1;
                //Temp solution
                boolean sent = false;
                while(!App.isClosed) {
                    int messagesCount = (new File("emails\\" + emailAddress + "\\").listFiles().length);
                    while(currentMessageIndex <= messagesCount) {
                        BufferedReader fileReader =
                                new BufferedReader(new FileReader(new File("emails\\" + emailAddress + "\\" + Integer.toString(currentMessageIndex) + ".txt")));
                        String emailContent = "";
                        String emailReaderContent;
                        sent = false;
                        while((emailReaderContent = fileReader.readLine()) != null) {
                            if(emailReaderContent.contains("From") && emailReaderContent.contains(emailAddress))
                                sent = true;
                            emailContent += emailReaderContent + "\n";
                        }
                        fileReader.close();
                        Message currentMessage = new Message(emailContent);
                        //TODO Maybe it's NOT OPTIMAL !!

                        int index = 0;
                        if(App.newFetch){
                            index = 0;
                        }

                        if(folder.getType() == Folder.FolderType.ALL_MAILS) {
                            folder.addMessage(currentMessage);
                            emailFolder.addEmail(index, currentMessage);
                        }
                        if(folder.getType() == Folder.FolderType.SENT) {
                            if(sent) {
                                folder.addMessage(currentMessage);
                                emailFolder.addEmail(index, currentMessage);
                            }
                        }
                        else if(folder.getType() == Folder.FolderType.INBOX) {
                            if(!sent) {
                                folder.addMessage(currentMessage);
                                emailFolder.addEmail(index, currentMessage);
                            }
                        }

                        currentMessageIndex++;

                    }
                    System.out.println("Folder: " + folder.getName() + " is updated!!");
                    System.err.println("Folder: " + folder.getName() + " has: " + folder.getMessageCount() + " messages");
                    try {
                        Thread.sleep(TIMER);
                    } catch(InterruptedException ex) {
                        ex.printStackTrace();
                        App.isClosed = true;
                    }
                }
                return null;
            }
        };
    }
}
