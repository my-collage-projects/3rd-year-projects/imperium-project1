package com.barosanu.services.IMAPServices;

import com.barosanu.App;
import com.barosanu.controller.ModelAccess;
import com.barosanu.model.EmailAccountBean;
import com.barosanu.model.EmailConstants;
import com.barosanu.model.Message;
import com.barosanu.model.Protocols;
import com.barosanu.model.folder.EmailFolderBean;
import com.barosanu.services.ProtocolRequirementsService;
import javafx.concurrent.Task;

/**
 *
 * @author Cezar
 */
public class IMAPRetrievalService extends ProtocolRequirementsService {

    private static int TIMER = 5000;

    private ModelAccess modelAccess;

    private EmailFolderBean<String> folderRoot;

    EmailFolderBean<String> accountFolder;


    public IMAPRetrievalService(EmailAccountBean userAccount , ModelAccess modelAccess ,EmailFolderBean<String> folderRoot) {
        super(userAccount);

        accountFolder  = new EmailFolderBean<>(userAccount.getEmailAddress());

        this.modelAccess = modelAccess;
        state = EmailConstants.LOGIN_STATE_SUCCEEDED;
        this.folderRoot = folderRoot;
    }



    @Override
    protected Task<Integer> createTask() {
        return new Task<Integer>() {
            @Override
            protected Integer call() throws Exception {
                state = establishConnection(Protocols.IMAP);
                if(state == EmailConstants.CONNECTION_FAILED)
                    return state;

                String serverResponse = readServerResponse("OK");

                writeClientResponse("a LOGIN " + userAccount.getEmailAddress() + " " + userAccount.getPassword());
                serverResponse = readServerResponse("OK");
                System.out.println(serverResponse);

                int numberOfInboxMessages = 0;
                int fetchedMessagesIndex = 1;
                String[] foldersNames = new String[3];
                //foldersNames[0] = "INBOX";
                foldersNames[0] = "\"[Gmail]/All Mail\"";
                //foldersNames[1] = "[GMAIL]\\"


                //EmailFolderBean<String> folderBean = (EmailFolderBean<String>) folderRoot.getChildren().get(2);

                EmailFolderBean<String> folderBean = new EmailFolderBean<String>(userAccount.getFolders().get(2).getName(),userAccount.getFolders().get(2).getFullName());

                folderRoot.getChildren().add(accountFolder);

                accountFolder.getChildren().add(folderBean);

                folderBean.setExpanded(true);


                while(!App.isClosed) {
                    //TODO i < 3
                    for(int i = 0 ; i < 1 ; i++) {
                        writeClientResponse("a SELECT " + foldersNames[i]);
                        while(!(serverResponse = in.readLine()).contains("Success")) {
                            System.out.println(serverResponse);
                            if(serverResponse.contains("EXISTS")) {
                                numberOfInboxMessages = Integer.parseInt(serverResponse.split(" ")[1]);
                            }
                        }
                        System.out.println(numberOfInboxMessages);
                        while(fetchedMessagesIndex <= numberOfInboxMessages) {
                            writeClientResponse("a FETCH " + fetchedMessagesIndex + " (BODY[])");
                            System.out.println(readServerResponse("FETCH"));
                            System.out.println("a FETCH " + fetchedMessagesIndex + " " + foldersNames[0]);
                            String messageContent = "";
                            while(!(serverResponse = in.readLine()).contains("Success")) { //TODO :
                                messageContent += serverResponse;
                            }
                            Message currentMessage = new Message(messageContent);

                            System.out.println(currentMessage.getSubject() + "////////////////////");

                            userAccount.getFolders().get(2).addMessage(currentMessage);
                            modelAccess.addFolder(userAccount.getFolders().get(2));



                            folderBean.addEmail(0,currentMessage);

                          /*  modelAccess.getSelectedFolder().getChildren().add(item);
                            item.setExpanded(true);
*/



                            fetchedMessagesIndex++;
                            System.out.println("-----------------------------------------------------------");
                            System.out.println(messageContent);
                            System.out.println("-----------------------------------------------------------");
                        }
                    }
                    try {
                        Thread.sleep(TIMER);
                    } catch(InterruptedException ex) {
                        ex.printStackTrace();
                    }
                }

                return state;


            }
        };
    }

    public String readServerResponse(String responseCode) throws Exception {
        // Although The String (serverResponse) Is Not Used Outside This Method,
        // We Kept It For Exception Reasons, For Example: If The Server Responses
        // With An Unexpected Response And We Want To Handle It From The Outside
        String serverResponse;
        while((serverResponse = in.readLine()) != null) {
            if(serverResponse.contains(responseCode))
                break;
        }
        return serverResponse;
    }

}
