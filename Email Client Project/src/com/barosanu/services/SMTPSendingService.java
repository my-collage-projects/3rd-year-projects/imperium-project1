package com.barosanu.services;

import com.barosanu.model.EmailAccountBean;
import com.barosanu.model.EmailConstants;
import com.barosanu.model.Protocols;
import javafx.concurrent.Task;

import java.net.InetAddress;
import java.util.Base64;

/**
 * @Author Raghad Hanna
 *
 * This Class Is Responsible For Sending An Email From One Email Account
 * To Another, It Speaks The SMTP Language To Communicate With The Servers
 */

public class SMTPSendingService extends ProtocolRequirementsService {
    private String subject;
    private String recipientAddress;
    private String content;

    public SMTPSendingService(EmailAccountBean senderAccount, String subject,
                              String recipientAddress, String content) {
        super(senderAccount);
        this.subject = subject;
        this.recipientAddress = recipientAddress;
        this.content = content;
    }

    @Override
    protected Task<Integer> createTask() {
        return new Task<Integer>() {
            @Override
            protected Integer call() {
                    state = establishConnection(Protocols.SMTP);

                    // In Case The State Is CONNECTION_FAILED,
                    // That Implies That The socket, in Or out objects Are null,
                    // Then The Connection Has Failed & We Can't Continue
                    // The Process Of Sending Any Further
                    if(state == EmailConstants.CONNECTION_FAILED)
                        return state;

                // 3rd Step: Data Exchange Between End Hosts
                if(socket != null && out != null && in != null) {
                    try {
                        // Server: "HELO"
                        String responseLine;
                        responseLine = readServerResponse("220");

                        // Client: "HELO"
                        writeClientResponse("HELO " + InetAddress.getLocalHost().getHostAddress());

                        // Server: "At Your Service"
                        responseLine = readServerResponse("250");

                        // Client: "AUTH LOGIN" (Authenticating The Credentials)
                        writeClientResponse("AUTH LOGIN");

                        // Encoding The Username With Base64 Is A Must
                        String encodedString = encodeWithBase64(userAccount.getEmailAddress());
                        // Client: Here's The Username
                        writeClientResponse(encodedString);

                        // Server: OK (In Base64)
                        in.readLine();

                        // Encoding The Password With Base64 Is A Must
                        encodedString = encodeWithBase64(userAccount.getPassword());
                        // Client: Here's The Password
                        writeClientResponse(encodedString);

                        // Server: OK (In Base64)
                        in.readLine();

                        // Client: "Here's The Sender Email Address"
                        writeClientResponse("MAIL FROM: <" + userAccount.getEmailAddress() + ">");

                        responseLine = readServerResponse("250");

                        // Client: "Here's The Recipient Email Address"
                        writeClientResponse("RCPT TO: <" + recipientAddress + ">");

                        responseLine = readServerResponse("250");

                        // Client: "Starting To Send The Actual Content Of The Message"
                        writeClientResponse("DATA");

                        responseLine = readServerResponse("354");

                        writeClientResponse("From: " + userAccount.getEmailAddress());
                        writeClientResponse("To: " + recipientAddress);
                        writeClientResponse("Subject: " + subject);

                        // This Tells The Server That The Following Content
                        // Should Be Rendered As HTML Document
                        writeClientResponse("Content-Type: text/html; charset=\"UTF-8\"\n");
                        writeClientResponse(content + "\n");
                        writeClientResponse(".");

                        responseLine = readServerResponse("250");

                        // Client: "I'm Done Communicating And I Want To Quit"
                        writeClientResponse("QUIT");

                        // Server: OK, We're Gonna Close The Connection
                        responseLine = readServerResponse("221");

                        // Closing The Connection Safely
                        closeConnection();
                        state = EmailConstants.MESSAGE_SENT_OK;
                    }
                    catch (Exception e) {
                        // Hopefully, There Won't Be Any Exception Thrown
                    }
                }
                return state;
            }
        };
    }

    public String readServerResponse(String responseCode) throws Exception {
        // Although The String (serverResponse) Is Not Used Outside This Method,
        // We Kept It For Exception Reasons, For Example: If The Server Responses
        // With An Unexpected Response And We Want To Handle It From The Outside
        String serverResponse;
        while((serverResponse = in.readLine()) != null) {
            if(serverResponse.contains(responseCode))
                break;
        }
        return serverResponse;
    }

    public String encodeWithBase64(String stringToBeEncoded) {
        return Base64.getEncoder().encodeToString(stringToBeEncoded.getBytes());
    }
}



