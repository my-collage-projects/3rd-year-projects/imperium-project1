package com.barosanu;

import com.barosanu.controller.ConfirmBox;
import com.barosanu.view.ViewFactory;
import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

import javax.naming.OperationNotSupportedException;
import java.io.*;
import java.util.ArrayList;

public class App extends Application{

	/**
	 * Temporary for testing
	 */
	public static int mainLayout;
	/**
	 *
	 */

	public static boolean newFetch = false;

	public static String userFirstName ="";
	public static String userLastName = "";

	public static boolean isClosed = false;


	private static ViewFactory viewFactory;
	private static Stage MainStage;

	public static void main(String[] args) {
		launch(args);
	}

	public static void addAccountToFile(String emailAddress,String password) {
		//TODO : Save a new account info to a file #storing
		/////////////////////////////////
		File file = new File("userAccounts.txt");
		try {
			if (!file.exists()) {
				file.createNewFile();
			}
			PrintWriter writer = new PrintWriter(new FileWriter(file));
			writer.println(emailAddress);
			writer.flush();
			writer.println(password);
			writer.close();

		} catch (IOException e) {
			e.printStackTrace();
		}
		/////////////////////////////////

	}

	public static void updateUserInfo(int i) {
//TODO

	}


	public static void startMainView() {

 		try {
			Scene scene = viewFactory.getMainScene();
			MainStage.setScene(scene);
			ViewFactory.addWindow(MainStage);
			MainStage.show();
			MainStage.setOnCloseRequest(event -> {
				event.consume();
				closeMainWindow();
			});
		} catch (OperationNotSupportedException e) {
			e.printStackTrace();
		}
	}

	private static void closeMainWindow() {
		MainStage.setOnHidden(event -> {
					isClosed = true;
					closeAllWindows();
				}
		);
		closeWindow(MainStage);
	}

	private static void closeAllWindows() {
		ArrayList<Stage> windows = ViewFactory.getWindows();
		for(Stage window : windows){
			window.close();
		}
	}


	@Override
	public void start(Stage primaryStage) throws Exception {

		MainStage = primaryStage;
		viewFactory = ViewFactory.defaultFactory;

		if(loadUserInfo() == -1){
			Scene ConfigUserInfo = viewFactory.getConfigureUserInfoScene();
			Stage UserConfigurationStage = new Stage();
			UserConfigurationStage.setScene(ConfigUserInfo);
			UserConfigurationStage.setOnCloseRequest(event -> {
				event.consume();
				closeWindow(UserConfigurationStage);
			});

			ViewFactory.addWindow(UserConfigurationStage);
			UserConfigurationStage.show();

		}
		else {
			startMainView();
		}
		//TODO: view a welcoming scene

	}



	private static void closeWindow(Stage window) {
		Boolean answer = ConfirmBox.display("Title" , "Sure you want to close ?");
		if(answer){
			//TODO : must save data before closing
 			window.close();
 			ViewFactory.removeWindow(window);
		}
	}

	public static int userHasAccounts() {
		// TODO : Read user's saved accounts info #storing
		/////////////////////////////////
		File file = new File("userInfo.txt");
		if (file.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(file));
				//TODO: maybe the file will be empty
				int numOfAccount = 0;
				numOfAccount = Integer.parseInt(reader.readLine());
				if (numOfAccount == -1) {
					System.out.println("User info not found");
					return -1;
				} else {
					System.out.println(numOfAccount);
					return numOfAccount;
				}
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		return -1;
	}

	private static int loadUserInfo(){
		//TODO : Read user info #storing
		/////////////////////////////////
		File file = new File("userInfo.txt");
		try {
			if(!file.exists()){
				file.createNewFile();
				PrintWriter writer = new PrintWriter(new FileWriter(file));
				writer.println(-1);
				writer.flush();
				writer.close();
				return -1;
			}
			else {
				//TODO: maybe the file will be empty
				BufferedReader reader = new BufferedReader(new FileReader(file));
				int numOfAccount = Integer.parseInt(reader.readLine());
				if(numOfAccount == -1 ){
					System.out.println("User info not found");
					return -1;
				}
				else{
					userFirstName = reader.readLine();
					userLastName = reader.readLine();
					if(numOfAccount > 0){

					}
					return numOfAccount;
				}
			}

		} catch (IOException e) {
			e.printStackTrace();
		}
		return -1;
	}

}