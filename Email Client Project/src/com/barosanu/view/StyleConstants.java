package com.barosanu.view;

public class StyleConstants {

    public static final String DEFAULT_CSS = "style.css";
    public static final String DARK_CSS = "styleDark.css";

    public static final String APPLICATION_ICON  = "file:resources/images/icon.png";

    public static final String AVATAR_IMAGE = "file:resources/images/avatar.jpg";

    public static final String INBOX_FOLDER_ICON = "images/inbox.png";
    public static final String SENT_FOLDER_ICON = "images/sent2.png";
    public static final String SPAM_FOLDER_ICON = "images/spam.png";
    public static final String EMAIL_FOLDER_ICON = "images/email.png";
    public static final String NORMAL_FOLDER_ICON = "images/folder.png";


}
