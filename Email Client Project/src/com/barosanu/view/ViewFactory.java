package com.barosanu.view;

import com.barosanu.controller.*;
import javafx.fxml.FXMLLoader;
import javafx.scene.Node;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.AnchorPane;
import javafx.stage.Stage;

import javax.naming.OperationNotSupportedException;
import java.io.IOException;
import java.util.ArrayList;

//it will have the access to the view elements
//and it will return different scenes for every layout
public class ViewFactory {


    //TODO :
    private ArrayList<String> openedTabs = new ArrayList<>();

    private static ArrayList<Stage> windows = new ArrayList<>();

    public static ViewFactory defaultFactory = new ViewFactory();
    private static boolean mainViewInitialized = false;

    private String theme = StyleConstants.DEFAULT_CSS;

    private final String EMAIL_DETAILS_FXML = "EmailDetailsLayout.fxml";
    private final static String MAIN_SCREEN_FXML = "MainLayout.fxml";
    private final String COMPOSE_SCREEN_FXML = "ComposeMessageLayout.fxml";
    private final String CONFIGURE_USER_INFO_FXML = "ConfigureUserInfoLayout.fxml";
    private final String ADD_A_NEW_ACCOUNT_FXML = "AddANewAccountLayout.fxml";

    private ArrayList<Scene> scenes = new ArrayList<>();

    //The only reference to ModelAccess Obj
    private ModelAccess modelAccess = new ModelAccess();

    public static void removeWindow(Stage stage) {
        windows.remove(stage);
    }

    public static ArrayList<Stage> getWindows() {
        return windows;
    }

    public Scene getMainScene() throws OperationNotSupportedException {
        if (!mainViewInitialized) {
            MainController mainController = new MainController(modelAccess);
            ModelAccess.setMainController(mainController);
            mainViewInitialized = true;
            return initializeScene(MAIN_SCREEN_FXML, mainController);
        }
        else{
            throw new OperationNotSupportedException("Main Scene Allready initialized");
        }
    }

    public Scene getComposeMessageScene(){
        AbstractController composeController = new ComposeMessageController(modelAccess);
        return initializeScene(COMPOSE_SCREEN_FXML,composeController);
    }

    public Scene getEmailDetailsScene(){
        EmailDetailsController emailDetailsController = new EmailDetailsController(modelAccess);
        return initializeScene(EMAIL_DETAILS_FXML ,emailDetailsController);
    }

    public Scene getConfigureUserInfoScene(){
        ConfigureUserInfoController configureUserInfoController =
                new ConfigureUserInfoController(modelAccess);
        return initializeScene(CONFIGURE_USER_INFO_FXML,configureUserInfoController);
    }

    public Scene getAddNewAccountScene(){
        AddANewAccountController addANewAccountController =
                new AddANewAccountController(modelAccess);
        return initializeScene(ADD_A_NEW_ACCOUNT_FXML, addANewAccountController);
    }

    public Scene getErrorScene(String errorMessage) {
        //TODO : we can use Alert class instead
        AnchorPane root = new AnchorPane();
        Label errorLabel = new Label(errorMessage);
        root.getChildren().add(errorLabel);
        Scene scene = new Scene(root);
        return scene;
    }

    private Scene initializeScene(String fxmlPath , AbstractController controller){
        FXMLLoader loader;
        Parent parent;
        Scene scene;
        try {
            loader = new FXMLLoader(getClass().getResource(fxmlPath));
            loader.setController(controller);
            parent = loader.load();
        } catch (IOException e) {
            System.out.println("IO Exception");
            return null;
        }
        scene = new Scene(parent);
        scene.getStylesheets().add(getClass().getResource(theme).toExternalForm());
        scenes.add(scene);
        return scene;
    }

    public void deleteScene(Scene scene){
        scenes.remove(scene);
    }

    public void setTheme(String theme){
        this.theme = theme;
        for(Stage window : windows){
            window.getScene().getStylesheets().add(getClass().getResource(theme).toExternalForm());
        }
    }

    //We can Use a helper method to add resolve Icons for the Tree Items
    public Node resolveIcon(String treeItemValue){
        String lowerCaseTreeItemValue = treeItemValue.toLowerCase();
        ImageView returnIcon;
        try {
            if(lowerCaseTreeItemValue.contains("inbox")){
                returnIcon= new ImageView(new Image(getClass().getResourceAsStream(StyleConstants.INBOX_FOLDER_ICON)));
            } else if(lowerCaseTreeItemValue.contains("sent")){
                returnIcon= new ImageView(new Image(getClass().getResourceAsStream(StyleConstants.SENT_FOLDER_ICON)));
            } else if(lowerCaseTreeItemValue.contains("spam")){
                returnIcon= new ImageView(new Image(getClass().getResourceAsStream(StyleConstants.SPAM_FOLDER_ICON)));
            } else if(lowerCaseTreeItemValue.contains("@")){
                returnIcon= new ImageView(new Image(getClass().getResourceAsStream(StyleConstants.EMAIL_FOLDER_ICON)));
            } else{
                returnIcon= new ImageView(new Image(getClass().getResourceAsStream(StyleConstants.NORMAL_FOLDER_ICON)));
            }
        } catch (NullPointerException e) {
            System.out.println("Invalid image location!!!");
            e.printStackTrace();
            returnIcon = new ImageView();
        }

        returnIcon.setFitHeight(16);
        returnIcon.setFitWidth(16);

        return returnIcon;
    }

    public static void addWindow(Stage window){
        windows.add(window);
        window.getIcons().add(new Image(StyleConstants.APPLICATION_ICON));
    }

    public void addOpenedTab(String id){
        System.out.println(id);
        openedTabs.add(id);
    }

    public boolean tabIsOpened(String id){
        return openedTabs.contains(id);
    }


    public void removeOpenedTab(String tabId) {
        openedTabs.removeIf(x -> x.equals(tabId));
    }
}
