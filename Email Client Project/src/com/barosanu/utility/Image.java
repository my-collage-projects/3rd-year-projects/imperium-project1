package com.barosanu.utility;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.awt.image.ColorModel;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.IOException;

public class Image {
    private static final String path = "C:/Users.../src/7horses.jpg";
    private static final File file = new File(path);

    public static BufferedImage deepCopy(BufferedImage bi , String name) throws IOException {
        ColorModel cm = bi.getColorModel();
        boolean isAlphaPremultiplied = cm.isAlphaPremultiplied();
        WritableRaster raster = bi.copyData(null);
        BufferedImage cImg = new BufferedImage(cm, raster, isAlphaPremultiplied, null);
        File saveImage = new File("resources/images/" + name +".jpg");
        ImageIO.write(cImg, "jpg", saveImage);
        return cImg;
    }

    public static void main(String[] args) throws IOException {
        BufferedImage cp, img;
        img = ImageIO.read(file);
        cp = deepCopy(img,"temp");
    }
}